public class Projekt_6 {

	public static void main(String[] args) {
		Projekt_6 problem = new Projekt_6();
		problem.suma();
	}
	/* Public :Find difference between the sum of the squares and the square of the sum
	 *   sum1 = Sum of the squares
	 *   sum2= Square of the sum  
	 *   sum1-sum2 = difference
	 *   return difference
	 */
	public static int suma() {
		{
			int total = 0;
			int tot = 0;
			int diff = 0;
			for (int i = 1; i <= 100; i++) {
				total += (i * i);
				tot += i;
				diff = (tot * tot) - total;
			}

			System.out.println("sum_squares() = " + total);
			System.out.println("squares_sum() = " + tot);
			System.out.println("difference= " + diff);
			return diff;
		}
	}
}
